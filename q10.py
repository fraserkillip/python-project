'''
HANGMAN v1.0
Fraser Killip  - fkil326
   _______
  |/      |
  |      (_)
  |      \|/
  |       |
  |      / \
  |
__|___
'''

import random

# A map of the chars and display order for the game picture thing
hang_map = [((' ',0),(' ',0),(' ',0),('_',3),('_',3),('_',3),('_',3),('_',3),('_',3),('_',3),(' ',0),(' ',0)),
            ((' ',0),(' ',0),('|',2),('/',4),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),('|',5),(' ',0)),
            ((' ',0),(' ',0),('|',2),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),('(',6),('_',6),(')',6)),
            ((' ',0),(' ',0),('|',2),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),('\\',6),('|',6),('/',6)),
            ((' ',0),(' ',0),('|',2),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),('|',6),(' ',0)),
            ((' ',0),(' ',0),('|',2),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),('/',6),(' ',0),('\\',6)),
            ((' ',0),(' ',0),('|',2),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0)),
            (('_',1),('_',1),('|',1),('_',1),('_',1),('_',1),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0),(' ',0))]

def print_map(fails):
    """Prints the game map thing to the screen"""
    for line in hang_map:
        string = ''
        for char in line:
            string += char[0] if char[1] <= fails else ' '
        print string

# All of the words available to us 
words = []
# Read all the words in from the text file
with open("wordlist.txt") as f:
    content = f.readlines()
    for line in content:
        # Trim off the new lines
        words.append(line.replace("\n",""))

def get_word():
    '''Gets a random word from the list of words'''
    return random.choice(words)
    
def hide_word(word, letters):
    text = ""
    for letter in word:
        text += letter if letter in letters else "_"
    return text

def clear_screen():
    '''Clear the content on the screen'''
    print '\x1b[H\x1b[2J'

def get_play_again():
    '''Asks the user if they want to play again'''
    val = raw_input("\nPlay again (y/n)? ")[0]
    return val if len(val) > 0 else 'n'
    
# Pregame welcome
clear_screen()
print "\n\nWelcome to Hang-a-man!\n\n"
name = raw_input("Please your name: ")

# How many times the user input an incorrect letter
deaths = 0


# The guessed letters
letters = []

# The word they are trying to guess
word = get_word()

# Game loop
while True:
    # Print the game screen
    clear_screen()
    print "Playing as ", name, " -  type 'exit' to quit."
    print_map(deaths)
    print "\nLetters guessed so far:"
    chars = ""
    for char in letters:
        chars += char + " "
    print "\t", chars, "\n"
    print "Current word: ", hide_word(word, letters)
    
    # Get their next guess
    text = raw_input("Please enter your next guess: ")
    
    # If they entered nothing
    if(len(text) == 0): continue
    
    # If the type exit, quit the game
    if text == "exit":
        break;
    
    # Game win
    if text == word:
        # Print display stuff
        clear_screen()
        print "You win!!! Yay!!!!\n"
        print "The word was: ", word
        if get_play_again() == "y":
            # Reset vars
            word = get_word()
            letters = []
            deaths = 0
            continue
        else:
            break
   
    # If the have a guess that's not in the word increase the deaths         
    if not text[0] in word:
        deaths += 1
    
    # If they have ran out of guesses
    if deaths == 6:
        clear_screen()
        print name, "has been hanged - you lose!"
        print_map(deaths)
        print "The correct word was: ", word
        if get_play_again() == "y":
            # Reset vars
            word = get_word()
            letters = []
            deaths = 0
            continue
        else:
            break
    
    # Add the guess to their previous guesses
    text = text[0]
    if not text in letters:
        letters.append(text)

clear_screen()
print "Goodbye! :(\n\n"