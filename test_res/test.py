import subprocess

def q1():
    """Test Question One"""
    print "Testing question one..."
    with open("q1words.csv") as f:
        content = f.readlines()
        linecount = 0
        passcount = 0
        for line in content:
            linecount += 1
            vals = line.split(',')
            vals[1] = vals[1].replace("\n","").replace("\r","") #Remove the newline chars
            output = subprocess.check_output("python ../q1.py "+vals[0], shell=True).replace("\n","").replace("\r","")
            if not vals[1] == output :
                print "\033[91mERROR\033[0m with input: '" + vals[0] + "'\r\n\texpected: '" + vals[1] + "'\r\n\treceived: '" + output + "'"
            else:
                passcount += 1
    print "Done! Passed " + str(passcount) + " of " + str(linecount)
    
q1()