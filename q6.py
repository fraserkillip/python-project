import re

def isPalindrome(string):
    '''Checks is a string or list type is palindromic'''
    # Clean up input
    string = re.sub("\W", "", string)
    # Loop over a range from the start to the middle of the list
    strlen = len(string)
    for i in range(strlen/2):
        # If any of the characters do not match the character on the opposide 'side' of the list
        if (not string[i] == string[strlen-i-1]):
            return False
    # If we get to here it is a palindrome
    return True