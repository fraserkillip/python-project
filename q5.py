"""
We weren't asked to write a primality tester, just something that uses one
is_prime() is adapted with permission from:
    http://stackoverflow.com/a/15285588
"""
def is_prime(n):
    # Two and three are the lowest primes so just check manually
    if n == 2 or n == 3: return True
    
    # Check if n is a multiple of two
    if n < 2 or n%2 == 0: return False
    
    # ALl numbers below 9 that aren't a multiple of two are prime
    if n < 9: return True
    
    # Check if it is a multiple of three
    if n%3 == 0: return False
    
    # We only need to check up until sqrt(n) with our dividers
    r = int(n**0.5)
    
    # Start at 5
    f = 5
    while f <= r:
        # Check is n is a multiple of n or n+2
        if n%f == 0: return False
        if n%(f+2) == 0: return False
        # This is a magic number which allows us to skip heaps of numbers :D
        f += 6
    
    # If we still haven't returned by now there are no other checks so it must be prime
    return True

# Get the users input
count = int(raw_input("Enter the number to find the primes below:"))

# Prep the output
output = ""
# Loop until N and check every number to see if it is prime! Wooooo #efficiency
for i in range(count) :
    if(is_prime(i)): output += " " + str(i)
    
print output