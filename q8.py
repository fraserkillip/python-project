import re # Woo regex!!! Bonus marks for my amazing regex? :D :D

def get_gradient(function):
    # Find all matches of my amazing regex
    gradients = re.findall("[+-]?\d*\*?x",function)
    grad = 0
    for val in gradients:
        # Clean up coefficients
        val = val.replace("*",'')
        # Make sure it is a valid match
        if len(val) == 0 : continue
        # Clean up coefficients
        val = '1' if val == 'x' else val.replace('x','')
        # Check to see if our coefficient is just a + or minus sign and append a one to it to prevent errors
        grad += int(val + "1" if val == "-" or val == "+" else val)
    return grad

def get_shift(function):
    # Find all matches of my amazing regex
    shifts = re.findall("[+-]?\d+(?=\+|-|\b|\Z)",function)
    shift = 0
    for val in shifts:
        # Make sure it is a valid match
        if len(val) == 0 : continue
        # Check to see if our coefficient is just a + or minus sign and append a one to it to prevent errors
        shift += int(val+"1" if val == "-" or val == "+" else val)
    return shift
    
def get_y_coef(function):
    # Find all matches of my amazing regex
    coefs = re.findall("[+-]?\d*y",function)
    coef = 0
    for val in coefs:
        # Make sure it is a valid match
        if len(val) == 0 : continue
        # Clean up coefficients
        val = '1' if val == 'y' else val.replace('y','')
        # Check to see if our coefficient is just a + or minus sign and append a one to it to prevent errors
        coef += int(val+"1" if val == "-" or val == "+" else val)
    return coef

def sequencer(function):
    # Strip out any spaces
    function = function.replace(" ","")
    
    # Split into two sides of the equation
    sides = function.split('=')
    left_side = sides[0]
    right_side = sides[1]
    
    # Check if we have a y on the right side and swap them
    # Code assumes there is a y on the left, doesn't matter is there is one on the right too
    if "y" in right_side:
        temp = left_side
        left_side = right_side
        right_side = temp
    
    # Set up coefficients
    gradient = 0
    shift = 0
    y_coefficient = 0
    
    # Calculate the gradient (x coefficient)
    try:
        # We essentially shift all the x terms to the right side so we negate them
        gradient -= get_gradient(left_side)
        gradient += get_gradient(right_side)
    except Exception as e:
        return "Error: Could not process argument"
      
    # Calculate the y shift
    try:
        # We essentially shift all the y shifts to the right side so we negate them
        shift -= get_shift(left_side)
        shift += get_shift(right_side)
    except Exception as e:
        return "Error: Could not process argument"

    # Calculate the y coefficient
    try:
        y_coefficient += get_y_coef(left_side)
        # We essentially shift all the y terms to the left side so we negate them
        y_coefficient -= get_y_coef(right_side)
    except Exception as e:
        return "Error: Could not process argument"
    
    # Make sure the sum of all ys is not zero
    if(y_coefficient == 0):
        return "Error: Could not process argument"    
    
    # Loop x from 0 to 5 and append the result to a string
    return_val = ""
    for x in range(6):
        # If it isn't the first number, add a comma and space
        if(x != 0 ): return_val += ", "
        # Determine the floating point value of the node
        float_val = float(gradient*x)/y_coefficient + float(shift)/y_coefficient
        # Append the number, but convert it to an int if the decimal is 0
        return_val += str(int(float_val) if int(float_val) == float_val else float_val)
    
    return return_val