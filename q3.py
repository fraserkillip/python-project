# We aren't checking for valid input... no point if data will always be valid
input_string = raw_input("Please enter your values separated by a space:")

values = input_string.split(" ")

# Read the number of seats
blue_seats = int(values[0])
red_seats = int(values[1])
purple_seats = int(values[2])

# Read the vote percentage
blue_vote = float(values[3])
red_vote = float(values[4])
purple_vote = float(values[5])

# Calculate the total number of seats
total_seats = blue_seats + red_seats + purple_seats

# Calculate the percentage of seats for each party
blue_seat_percentage = float(blue_seats) / total_seats
red_seat_percentage = float(red_seats) / total_seats
purple_seat_percentage = float(purple_seats) / total_seats

# Check if blue wins solo
if(blue_seat_percentage > 0.5 and blue_vote > 0.51 ):
    print "Blue Party"
# Else check for red solo win
elif(red_seat_percentage > 0.5 and red_vote > 0.51 ):
    print "Red Party"
# Else check for purple solo win
elif(purple_seat_percentage > 0.5 and purple_vote > 0.51 ):
    print "Purple Party"
# Check for blue and purple combined win
elif((blue_vote > red_vote) and (blue_seat_percentage + purple_seat_percentage) > 0.5 and (blue_vote + purple_vote) > 0.51 ):
    print "Blue Party and Purple Party"
# Check for red and purple combined win
elif((blue_vote < red_vote) and (red_seat_percentage + purple_seat_percentage) > 0.5 and (red_vote + purple_vote) > 0.51 ):
    print "Red Party and Purple Party"
# No winner can be named
else:
    print "No Winner"