import re, string

def alphasort(in_list):
    regex = re.compile("^[a-zA-Z]*$")
    new_list = [string.lower() for string in in_list if regex.match(string) != None]
    do_sort(new_list, 0, len(new_list)-1, 0)
    return new_list

def charAt(string, index):
    return -1 if len(string) <= index else string[index]

def swap(in_list, first, second):
    temp = in_list[first]
    in_list[first] = in_list[second]
    in_list[second] = temp
 
def do_sort(in_list, lower, upper, depth):
    # If your upper bounds happens to be lower or equal to our lower bound, there is nothing to sort
    if upper <= lower: return
    # Create copies of our bounds so we can modify them
    lt = lower
    gt = upper
    # Set the pivot as the char in the `depth` position in the string, will be -1 if depth >= length of the string
    pivot = charAt(in_list[lower], depth)
    # Start from the next item after the lower (pivot)
    i = lower + 1
    # While we have items left to pivot (the index we are swapping is less than or equal to our current upper bound)
    while i <= gt:
        # Get the character to test against our pivot
        test_char = charAt(in_list[i], depth)
        
        # If the test char is less than the pivot
        if(test_char < pivot):
            # Swap if with our current 'lowest' item
            swap(in_list, lt, i)
            # Increment our 'lowest' value index
            lt += 1
            # Increment our current position
            i += 1
        # If the char is larger than the pivot
        elif test_char > pivot:
            # Swap the current item with the one at our upper 'bound'
            swap(in_list, i, gt)
            # Decrease our upper 'bound'
            gt -= 1
        else:
            # If it is the same as the pivot, leave it where it is
            i += 1
    
    # Sort the values below the pivot
    under = do_sort(in_list, lower, lt-1, depth)
    
    # If our pivot was an actual character
    if(pivot != -1):
        # Sort items on the pivot
        do_sort(in_list, lt, gt, depth +1)
    
    # Sor items above the pivot
    do_sort(in_list, gt+1, upper, depth)
    

def bad_sort(myList):
    end = len(myList)-1
    while (end!=-1):
        swapped=-1
        for i in range(0,end):
            if myList[i]>myList[i+1]:
                temp=myList[i]
                myList[i]=myList[i+1]
                myList[i+1]=temp
                swapped=i
        end=swapped
    return myList

def benchSort():
    import string
    import time
    import random

    count = 100
    
    
    print "Generating random test strings..."
    original = []
    for i in range(count):
        original.append([''.join(random.choice(string.ascii_lowercase) for _ in range(random.randint(5,15))) for _ in range(500)])
    print "Done!\nTesting sort function..."
    
    user = []
    for arr in original:
        arr = arr[:]
        user.append(arr)
    start = time.time()
    for i in range(count):
        user[i] = alphasort(user[i])
    usertime = (time.time() - start)/count
    
    bad = []
    for arr in original:
        arr = arr[:]
        bad.append(arr)
    start = time.time()
    for i in range(count):
        bad[i] = bad_sort(bad[i])
    badtime = (time.time() - start)/count

    system = []
    for arr in original:
        arr = arr[:]
        system.append(arr)
    start = time.time()
    for i in range(count):
        system[i].sort()
        
    systemtime = (time.time() - start)/count

    valid = True
    for i in range(len(user)-1):
        for j in range(len(user[i])-1):
            valid &= user[i][j] == system[i][j]
    if not valid: print "### ERROR NOT VALID ###"
    
    print "Done... Average times are as follows:"
    
    print "Your code:\t", usertime*1000, 'ms'
    print "Python .sort()\t", systemtime*1000, 'ms'
    
    syslonger = usertime/systemtime > 1
    bubblelonger = usertime/badtime > 1
    
    print "Your code is ", usertime/systemtime if syslonger else systemtime/usertime, "times","longer" if syslonger else "quicker","than .sort()"
    print "Your code is ", usertime/badtime if bubblelonger else badtime/usertime, "times","longer" if bubblelonger else "quicker","than Bubble Sort"