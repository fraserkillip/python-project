import math

# Get the user input
remaining = float(raw_input("Please enter your amount:")[1:6])

# We can't dispense anything lower than 10 cents, so round to 1dp
# We also multiply by ten to remove precision errors
remaining = round(remaining * 10, 0)

'''
Im so not commenting all of these... so ill comment them all here
Perform a floored division of the remaing money
Then modulus the remaining with the divisor to get the change remaining
'''
hundreds = int(remaining // 1000)
remaining = remaining % 1000

fifties = int(remaining // 500)
remaining = remaining % 500

twenties = int(remaining // 200)
remaining = remaining % 200

tens = int(remaining // 100)
remaining = remaining % 100

fives = int(remaining // 50)
remaining = remaining % 50

twos = int(remaining // 20)
remaining = remaining % 20

ones = int(remaining // 10)
remaining = remaining % 10

halves = int(remaining // 5)
remaining = remaining % 5

fifths = int(remaining // 2)
remaining = remaining % 2

tenths = int(remaining // 1)
remaining = remaining % 1

# Print everything
print "10c : " + str(tenths)
print "20c : " + str(fifths)
print "50c : " + str(halves)
print "$1  : " + str(ones)
print "$2  : " + str(twos)
print "$5  : " + str(fives)
print "$10 : " + str(tens)
print "$20 : " + str(twenties)
print "$50 : " + str(fifties)
print "$100: " + str(hundreds)
