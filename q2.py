def int_mult(a, b):
    """This function will multiply two numbers together as integers (rational numbers will be truncated)"""
    # We pass them as a float so that the number is always parsed from the string input
    # then we convert them into an int for multiplication.
    try:
        a = int(round(float(a)))
        b = int(round(float(b)))
        
        # Return the integer multiplication
        return a*b
    except Exception as e:
        return "Error: Invalid Argument Type"