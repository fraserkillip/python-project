import re,math

# Create a list for the grades
grades = []

# Open the file in read mode and process the lines
with open("grades.txt") as f:
    content = f.readlines()
    for line in content:
        # Get rid of stupid new line crap
        line = line.replace('\n','').replace('\r','')
        
        # Check if we have a mark or not
        if (re.search(',[+-]?\d', line) == None):
            print line
            continue
        
        # Split the value into name and mark
        vals = line.split(',')
        
        # Add it as a tuple so we can sort it
        grades.append((vals[0],int(vals[1])))

# Sort the grades
grades = sorted(grades, key=lambda student: student[1], reverse=True)

# Open the file in write mode
with open("grades.txt", "w") as f:
    # Loop through the top 5% rounded up
    for grade in grades[0:int(math.ceil(len(grades)*0.05))]:
        # Write the name and grade
        f.write(','.join(str(i) for i in grade))
        # Add a new line (windows safe)
        f.write("\r\n")

print "Data Processed"