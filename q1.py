# Read use input
input_string = raw_input("Please enter your string:")

# Define the transforms
transforms = [('a', '@'),('B', '8'),('c', '('),('E', '3'),('g', '9'),('h', '#'),('i', '1'),('I', '1'),('l', '1'),('o', '0'),('O', '0'),('s', '5'),('S', '5'),('t', '+'),('T', '7'),('z', '2'),('Z', '2')]

# Loop through the transforms and apply them
for transform in transforms:
    input_string = input_string.replace(transform[0],transform[1])

# Print output
print input_string